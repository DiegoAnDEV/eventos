﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eventos.Startup))]
namespace eventos
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
